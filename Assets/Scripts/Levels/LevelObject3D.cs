﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sokoban.Levels {
    public class LevelObject3D : MonoBehaviour {

        public void SetModel (GameObject item) {
            var obj = Instantiate (item) as GameObject;
            obj.transform.SetPositionAndRotation (Vector3.zero, Quaternion.identity);
            obj.transform.SetParent (transform, false);
        }
    }
}
