﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

namespace Sokoban.Levels {
    public class Level {
        [XmlAttribute (AttributeName = "Id")]
        public string Id;

        private int _width = -1;
        private int _height = -1;
        
        public int LevelWidth {
            get {
                if (_width < 0) {
                    LevelLines.ForEach (line => {
                        if (line.Length > _width) {
                            _width = line.Length;
                        }
                    });
                }
                return _width;
            }
        }
        public int LevelHeight {
            get {
                return LevelLines.Count;
            }
        }

        [XmlElement (ElementName = "L")]
        public List<string> LevelLines = new List<string> ();
    }

    [XmlRoot ("SokobanLevels")]
    public class SokobanLevels {

        [XmlElement ("Title")]
        public string Title;

        [XmlArray ("LevelCollection"), XmlArrayItem ("Level", typeof (Level))]
        public List<Level> Levels = new List<Level> ();
    }

    public class LevelParser : XMLParser {

        public SokobanLevels LoadLevelData(string text) {
            return Load<SokobanLevels> (text);
        }

        public SokobanLevels LoadLevelData (TextAsset asset) {
            return Load<SokobanLevels> (asset.text);
        }
    }

    public class XMLParser {

        public T Load<T> (string data) {
            XmlSerializer serializer = new XmlSerializer (typeof (T));
            var reader = new System.IO.StringReader (data);
            T deserialized = (T) serializer.Deserialize (reader);
            reader.Close ();
            return deserialized;
        }
    }
}