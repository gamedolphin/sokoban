﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Sokoban.Levels {
    public struct LevelInfo {
        public string fileName;
        public string levelID;
    }


    public class LevelManager {

        private const string LEVELSFOLDER = "LevelData";
        private LevelParser levelParser;
        TextAsset[] levelList;


        public LevelManager (LevelParser parser) {
            levelParser = parser;
        }

        public Level GetLevel(LevelInfo levelInfo) {
            var levelText = Resources.Load<TextAsset> (Path.Combine(LEVELSFOLDER, levelInfo.fileName));
            SokobanLevels levelSet =  levelParser.LoadLevelData (levelText);
            return levelSet.Levels.Find (level => {
                return level.Id == levelInfo.levelID;
            });
        }
    }
}