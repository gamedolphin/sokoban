﻿using Sokoban.Box;
using Sokoban.Game;
using Sokoban.Grid;
using Sokoban.Hole;
using Sokoban.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Sokoban.Levels {
    public class LevelDrawer {

        [Inject]
        private DiContainer container;
        
        private const string LEVELHOLDERSTRING = "LevelHolder";
        private Transform levelHolder;

        private GridManager gridManager;
        private GameManager gameManager;
        private LevelSpriteData spriteData;

        private Level levelData;
        private List<LevelObject> levelObjects = new List<LevelObject> ();
        private PlayerController playerController;

        private bool canPlaceFloor = false;

        private readonly Settings _settings;
        public Settings LevelSettings {
            get {
                return _settings;
            }
        }

        public LevelDrawer (GridManager gM, Settings settings) {
            gridManager = gM;
            _settings = settings;
        }

        public void DrawLevel ( GameManager manager, Level lData ) {
            gameManager = manager;
            spriteData = LevelSettings.SpriteData;
            ClearLevel ();
            SetupParentTransform (gameManager.transform);
            SetupGrid (lData);
            levelData = lData;
            var levelLines = levelData.LevelLines;
            for (int i = 0; i < levelLines.Count; i++) {
                string line = levelLines[i];
                canPlaceFloor = false;
                for (int j = 0; j < line.Length; j++) {
                    PlaceLevelObject (line[j], j, levelLines.Count - i - 1);
                }
            }
        }

        private void PlaceLevelObject (char type, int x, int y) {
            switch(type) {
                case ' ':
                    PlaceFloor (x, y);
                    break;
                case '#':
                    PlaceWall (x, y);
                    canPlaceFloor = true;
                    break;
                case '$':
                    PlaceBox (x, y);
                    PlaceFloor (x, y);
                    break;
                case '*':
                    PlaceHole (x, y);
                    PlaceBox (x, y);
                    break;
                case '.':
                    PlaceHole (x, y);
                    break;
                case '@':
                    PlacePlayer (x, y);
                    PlaceFloor (x, y);
                    break;
                default:
                    break;
            }
        }

        private void PlacePlayer(int x, int y) {
            if(playerController != null) {
                GameObject.Destroy (playerController.gameObject);
            }
            playerController = container.InstantiatePrefab (LevelSettings.playerPrefab).GetComponent<PlayerController> ();
            playerController.transform.SetParent (levelHolder);
            playerController.SetupPlayer (x, y);
        }

        private void PlaceFloor(int x, int y) {
            if(canPlaceFloor) {
                var obj = PlaceLevelObject (SpriteType.FLOOR1, x, y);
                levelObjects.Add (obj);
            }
        }

        private void PlaceHole (int x, int y) {
            var obj = PlaceLevelObject (SpriteType.HOLE1, x, y);
            levelObjects.Add (obj);
            gridManager.AddUnit (GridUnitType.HOLE, new GridPosition (x, y));
            obj.SetSortingLayer (k.SortingLayers.HOLE);
            HoleController hC = container.InstantiateComponent<HoleController> (obj.gameObject);
            hC.Initialize (x, y);
        }

        private void PlaceBox (int x, int y) {
            var obj = PlaceLevelObject (SpriteType.BOX1, x, y);
            levelObjects.Add (obj);
            gridManager.AddUnit (GridUnitType.BOX, new GridPosition (x, y));
            obj.SetSortingLayer (k.SortingLayers.BOX);
            BoxController bC = container.InstantiateComponent<BoxController> (obj.gameObject);
            bC.Initialize (x, y);
        }

        private void PlaceWall (int x, int y) {
            var obj = PlaceLevelObject (SpriteType.WALL1, x, y);
            levelObjects.Add (obj);
            obj.SetSortingLayer (k.SortingLayers.WALL);
            gridManager.AddUnit (GridUnitType.WALL, new GridPosition (x, y));
        }

        private void ClearLevel () {
            levelObjects.ForEach (obj => {
                GameObject.Destroy (obj.gameObject);
            });
            levelObjects.Clear ();
        }

        private void SetupParentTransform ( Transform parent ) {
            if (levelHolder == null) {
                var holder = new GameObject (LEVELHOLDERSTRING);
                holder.transform.SetParent (parent);
                levelHolder = holder.transform;
            }
        }

        private void SetupGrid (Level levelData) {
            gridManager.Initialize (levelData.LevelWidth, levelData.LevelHeight);
        }

        private LevelObject PlaceLevelObject (SpriteType t, int x, int y) {
            var levelObject = Object.Instantiate (LevelSettings.LevelObjectPrefab).GetComponent<LevelObject> ();
            levelObject.SetSprite(spriteData.spriteList[t]);
            levelObject.transform.SetParent (levelHolder, false);
            levelObject.transform.position = gridManager.GetPosition (x,y);
            return levelObject;
        }

        [System.Serializable]
        public class Settings {
            public LevelObject LevelObjectPrefab;
            public LevelSpriteData SpriteData;
            public PlayerController playerPrefab;
        }
    }
}
