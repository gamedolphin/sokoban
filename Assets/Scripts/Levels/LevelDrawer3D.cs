﻿using Sokoban.Game;
using Sokoban.Grid;
using Sokoban.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sokoban.Levels {
    public class LevelDrawer3D {

        private const string LEVELHOLDERSTRING = "LevelHolder";
        private Transform levelHolder;

        private GridManager gridManager;
        private LevelPrefabData prefabData;
        private Settings _settings;
        public Settings LevelSettings {
            get {
                return _settings;
            }
        }

        private Level levelData;
        private List<LevelObject3D> levelObjects = new List<LevelObject3D> ();
        private PlayerController playerController;

        private GameManager3D gameManager;

        private bool canPlaceFloor = false;

        public LevelDrawer3D (GridManager gM, Settings settings) {
            gridManager = gM;
            _settings = settings;
        }

        public void DrawLevel (GameManager3D manager, Level lData) {
            gameManager = manager;
            prefabData = LevelSettings.PrefabData;
            ClearLevel ();
            SetupParentTransform (gameManager.transform);
            SetupGrid (lData);
            levelData = lData;
            var levelLines = levelData.LevelLines;
            for (int i = 0; i < levelLines.Count; i++) {
                string line = levelLines[i];
                canPlaceFloor = false;
                for (int j = 0; j < line.Length; j++) {
                    PlaceLevelObject (line[j], j, levelLines.Count - i - 1);
                }
            }
        }

        private void PlaceLevelObject (char type, int x, int y) {
            switch (type) {
                case ' ':
                    PlaceFloor (x, y);
                    break;
                case '#':
                    PlaceWall (x, y);
                    canPlaceFloor = true;
                    break;
                case '$':
                    PlaceBox (x, y);
                    PlaceFloor (x, y);
                    break;
                case '*':
                    PlaceHole (x, y);
                    PlaceBox (x, y);
                    PlaceFloor (x, y);
                    break;
                case '.':
                    PlaceHole (x, y);
                    PlaceFloor (x, y);
                    break;
                case '@':
                    // PlacePlayer (x, y);
                    PlaceFloor (x, y);
                    break;
                default:
                    break;
            }
        }

        private void PlaceBox(int x, int y) {
            var obj = PlaceLevelObject (PrefabType.BOX, x, y);
            levelObjects.Add (obj);
            gridManager.AddUnit (GridUnitType.BOX, new GridPosition (x, y));
            //BoxController bC = container.InstantiateComponent<BoxController> (obj.gameObject);
            //bC.Initialize (x, y);
        }
        
        private void PlaceHole(int x, int y) {
            var obj = PlaceLevelObject (PrefabType.HOLE, x, y);
            levelObjects.Add (obj);
            gridManager.AddUnit (GridUnitType.HOLE, new GridPosition (x, y));
            // HoleController hC = container.InstantiateComponent<HoleController> (obj.gameObject);
            // hC.Initialize (x, y);
        }

        private void PlaceWall(int x, int y) {
            // levelObjects.Add (PlaceLevelObject (PrefabType.WALL, x, y));
            gridManager.AddUnit (GridUnitType.WALL, new GridPosition (x, y));
        }

        private void PlaceFloor (int x, int y) {
            if (canPlaceFloor) {
                levelObjects.Add (PlaceLevelObject (PrefabType.FLOOR, x, y));
            }
        }

        private LevelObject3D PlaceLevelObject (PrefabType t, int x, int y) {
            var levelObject = Object.Instantiate (LevelSettings.LevelObjectPrefab).GetComponent<LevelObject3D> ();
            levelObject.SetModel (prefabData.GetObjectOfType (t));
            levelObject.transform.SetParent (levelHolder, false);
            levelObject.transform.position = gridManager.GetPosition3D (x, y);
            return levelObject;
        }

        private void SetupGrid (Level levelData) {
            gridManager.Initialize (levelData.LevelWidth, levelData.LevelHeight);
        }

        private void SetupParentTransform (Transform parent) {
            if (levelHolder == null) {
                var holder = new GameObject (LEVELHOLDERSTRING);
                holder.transform.SetParent (parent);
                levelHolder = holder.transform;
            }
        }

        private void ClearLevel () {
            levelObjects.ForEach (obj => {
                GameObject.Destroy (obj.gameObject);
            });
            levelObjects.Clear ();
        }

        [System.Serializable]
        public class Settings {
            public LevelObject3D LevelObjectPrefab;
            public LevelPrefabData PrefabData;
            public PlayerController playerPrefab;
        }
    }
}