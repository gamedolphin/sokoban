﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sokoban.Levels {
    public enum PrefabType {
        NONE,
        WALL,
        BOX,
        HOLE,
        FLOOR
    }

    [System.Serializable]
    public class PrefabObject {
        public PrefabType Type;
        public List<GameObject> Object;
    }

    [CreateAssetMenu (menuName = "Sokoban/PrefabList")]
    public class LevelPrefabData : ScriptableObject {

        public List<PrefabObject> prefabList;

        public GameObject GetObjectOfType (PrefabType type, int n = -1) {
            PrefabObject obj = prefabList.Find (x => {
                return x.Type == type;
            });

            if (obj == null) {
                return null;
            }
            else {
                if (n < 0 || n > obj.Object.Count) {
                    return obj.Object[Random.Range (0, obj.Object.Count)];
                }
                else {
                    return obj.Object[n];
                }
            }
        }
    }
}