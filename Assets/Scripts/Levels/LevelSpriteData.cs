﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sokoban.Levels {
    public enum SpriteType {
        NONE,
        WALL1, WALL2, WALL3,
        BOX1, BOX2, BOX3,
        HOLE1, HOLE2, HOLE3,
        FLOOR1, FLOOR2, FLOOR3
    }

    [System.Serializable]
    public class DictionaryOfEnumAndSprite : SerializableDictionary<SpriteType, Sprite> { }

    [CreateAssetMenu (menuName = "Sokoban/SpriteList")]
    public class LevelSpriteData : ScriptableObject {

        public DictionaryOfEnumAndSprite spriteList;
    }
}