﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sokoban.Levels {
    [RequireComponent (typeof (SpriteRenderer))]
    public class LevelObject : MonoBehaviour {

        private SpriteRenderer spriteRenderer;

        public void SetSprite (Sprite spr) {
            if (spriteRenderer == null) {
                spriteRenderer = GetComponent<SpriteRenderer> ();
            }
            spriteRenderer.sprite = spr;
        }

        public void SetSortingLayer (int layerId) {
            if (spriteRenderer == null) {
                spriteRenderer = GetComponent<SpriteRenderer> ();
            }
            spriteRenderer.sortingLayerID = layerId;
        }
    }
}
