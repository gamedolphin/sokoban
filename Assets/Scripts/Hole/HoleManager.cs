﻿using Sokoban.Grid;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UniRx;

namespace Sokoban.Hole {
    public class HoleManager { 

        public List<HoleController> holes = new List<HoleController> ();

        public ReactiveProperty<int> FilledHoleCount {
            get; private set;
        }
        public ReactiveProperty<int> TotalHoleCount {
            get; private set;
        }
        public System.Action OnHoleFilled;
        public System.Action OnAllHolesFilled;

        public HoleManager () {
            FilledHoleCount = new ReactiveProperty<int> (0);
            TotalHoleCount = new ReactiveProperty<int> (0);
        }

        public void AddHole(HoleController hole) {
            holes.Add (hole);
            hole.isOccupied.SkipLatestValueOnSubscribe().Subscribe (OnHoleChanged).AddTo(hole);
            TotalHoleCount.Value += 1;
        }

        private void OnHoleChanged (bool value ) {
            FilledHoleCount.Value += value ? 1 : -1;
        }

        public void RemoveHole(HoleController hole) {
            holes.Remove (hole);
            if (hole.isOccupied.Value) {
                FilledHoleCount.Value -= 1;
            }
            TotalHoleCount.Value -= 1;
        }

        public void OnBoxMove (GridPosition oldPos, GridPosition newPos) {
            var filledHole = holes.Find (hole => {
                return hole.Position.Equals (newPos);
            });
            if (filledHole != null) {
                filledHole.isOccupied.Value = true;
            }
            else {
                var emptiedHole = holes.Find (hole => {
                    return hole.Position.Equals (oldPos);
                });
                if(emptiedHole != null) {
                    emptiedHole.isOccupied.Value = false;
                }
            }
        }
    }
}
