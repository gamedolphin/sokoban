﻿using Sokoban.Grid;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UniRx;

namespace Sokoban.Hole {
    public class HoleController : MonoBehaviour {

        [Inject]
        private HoleManager holeManager;

        public GridPosition Position;
        public ReactiveProperty<bool> isOccupied {
            get; private set;
        }

        private void Awake () {
            isOccupied = new ReactiveProperty<bool> (false);
            Debug.Log ("HERE AWAKE");
        }

        public void Initialize (int x, int y) {
            Position = new GridPosition(x, y);
            holeManager.AddHole (this);
            Debug.Log ("HERE INIT");
        }

        private void OnDestroy () {
            holeManager.RemoveHole (this);
        }
    }
}
