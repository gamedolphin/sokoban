﻿using Sokoban.Levels;
using UnityEditor;
using UnityEngine;


[CustomPropertyDrawer (typeof (DictionaryOfEnumAndSprite))]
public class AnySingleLineSerializableDictionaryPropertyDrawer : SingleLineSerializableDictionaryPropertyDrawer { }
