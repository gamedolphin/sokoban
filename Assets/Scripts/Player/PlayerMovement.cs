﻿using Sokoban.Grid;
using UnityEngine;
using Zenject;
using DG.Tweening;
using System.Collections.Generic;
using Sokoban.Box;

namespace Sokoban.Player {
    [RequireComponent(typeof(PlayerInput))]
    public class PlayerMovement : MonoBehaviour {

        [Inject]
        private GridManager grid;
        [Inject]
        private BoxManager boxManager;

        private PlayerInput playerInput;
        private bool isMoving = false;
        private GridPosition currentPosition;
        private List<GridUnitType> collisionFilter = new List<GridUnitType>();

        private void Awake () {
            playerInput = GetComponent<PlayerInput> ();
            collisionFilter.Add (GridUnitType.WALL);
            collisionFilter.Add (GridUnitType.BOX);
        }

        private void Start () {
            isMoving = false;
            playerInput.OnSwipe += MoveInDirection;
        }

        public void PlacePlayer (int x, int y) {
            currentPosition = new GridPosition (x, y);
            grid.AddUnit (GridUnitType.PLAYER, currentPosition);
        }

        private void MoveInDirection (SwipeDirection dir) {
            if (isMoving) {
                return;
            }
            int dX = 0;
            int dY = 0;
            switch(dir) {
                case SwipeDirection.DOWN:
                    dY = -1;
                    break;
                case SwipeDirection.UP:
                    dY = 1;
                    break;
                case SwipeDirection.LEFT:
                    dX = -1;
                    break;
                case SwipeDirection.RIGHT:
                    dX = 1;
                    break;
            }
            GridPosition dPos = new GridPosition (dX, dY);
            GridPosition posNew = currentPosition + dPos;
            GridUnit pos = grid.MoveUnit (GridUnitType.PLAYER, currentPosition, dPos, collisionFilter);
            if(pos.Units == null) {
                MoveToPosition (pos.GridPosition);
            }
            else {
                var collidedWith = pos.Units;
                if(collidedWith.Contains(GridUnitType.BOX)) {
                    BoxController box = boxManager.FindBox (posNew);
                    GridUnit collisionResponse = box.Move (dPos);
                    // Debug.Log (collisionResponse.Units);
                    if(collisionResponse.Units == null) {
                        grid.MoveUnit (GridUnitType.PLAYER, currentPosition, dPos, collisionFilter);
                        MoveToPosition (posNew);
                    }
                }
            }
        }

        private void MoveToPosition (GridPosition pos) {
            currentPosition = pos;
            isMoving = true;
        }

        private void OnDestroy () {
            playerInput.OnSwipe -= MoveInDirection;
        }
    }
}