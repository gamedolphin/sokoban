﻿using UnityEngine;

namespace Sokoban.Player {
    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(PlayerInput))]
    [RequireComponent(typeof(PlayerMovement))]
    public class PlayerController : MonoBehaviour {

        private PlayerMovement playerMovement;

        private void Awake () {
            playerMovement = GetComponent<PlayerMovement> ();
        }

        public void SetupPlayer(int x, int y) {
            playerMovement.PlacePlayer (x, y);
        }

    }
}
