﻿using UnityEngine;
using Lean.Touch;

namespace Sokoban.Player {

    public enum SwipeDirection {
        UP, DOWN, LEFT, RIGHT
    }

    public delegate void OnTouchSwipe (SwipeDirection dir);

    public class PlayerInput : MonoBehaviour {

        public event OnTouchSwipe OnSwipe;

        [SerializeField]
        private LeanTouch leanTouchPrefab;
        private LeanTouch leanTouchObject;

        private void OnEnable () {
            if(leanTouchObject == null) {
                leanTouchObject = ( Instantiate (leanTouchPrefab.gameObject) as GameObject ).GetComponent<LeanTouch> ();
                leanTouchObject.transform.SetParent (transform);
            }
            LeanTouch.OnFingerSwipe += OnFingerSwipe;
        }

        private void OnDisable () {
            LeanTouch.OnFingerSwipe -= OnFingerSwipe;
        }

        private void OnFingerSwipe (LeanFinger finger) {
            var swipe = finger.SwipeScreenDelta;
            SwipeDirection dir = SwipeDirection.DOWN;
            if (swipe.x < -Mathf.Abs (swipe.y)) {
                dir = SwipeDirection.LEFT;
            }

            if (swipe.x > Mathf.Abs (swipe.y)) {
                dir = SwipeDirection.RIGHT;
            }

            if (swipe.y < -Mathf.Abs (swipe.x)) {
                dir = SwipeDirection.DOWN;
            }

            if (swipe.y > Mathf.Abs (swipe.x)) {
                dir = SwipeDirection.UP;
            }

            if (OnSwipe != null) {
                OnSwipe (dir);
            }
        }

    }
}