//This class is auto-generated do not modify
namespace k
{
	public static class Scenes
	{
		public const string MAIN_MENU_SCENE = "MainMenuScene";
		public const string GAME_SCENE3D = "GameScene3D";

		public const int TOTAL_SCENES = 2;


		public static int nextSceneIndex()
		{
			var currentSceneIndex = UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex;
			if( currentSceneIndex + 1 == TOTAL_SCENES )
				return 0;
			return currentSceneIndex + 1;
		}
	}
}