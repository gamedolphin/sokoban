using Zenject;
using Sokoban.Levels;
using Sokoban.Grid;
using Sokoban.Box;
using Sokoban.Hole;
using Sokoban.Game;

public class GameInstaller3D : MonoInstaller<GameInstaller>
{
    [InjectOptional]
    public LevelInfo levelInfo = new LevelInfo {
        fileName = "001_Better",
        levelID = "1"
    };
    public override void InstallBindings()
    {
        Container.Bind<GridManager> ().AsSingle ().NonLazy ();
        Container.Bind<BoxManager> ().AsSingle ().NonLazy ();
        Container.Bind<HoleManager> ().AsSingle ().NonLazy ();
        Container.Bind<LevelParser> ().AsSingle ().NonLazy ();
        Container.Bind<LevelManager> ().AsSingle ().NonLazy ();
        Container.Bind<LevelDrawer3D> ().AsSingle ().NonLazy ();
        Container.BindInstance (levelInfo).WhenInjectedInto<GameManager3D> ();
    }
}