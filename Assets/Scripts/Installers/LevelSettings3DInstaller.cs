using Sokoban.Levels;
using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "LevelSettings3DInstaller", menuName = "Installers/LevelSettings3DInstaller")]
public class LevelSettings3DInstaller : ScriptableObjectInstaller<LevelSettings3DInstaller>
{

    public LevelDrawer3D.Settings LevelSettings;

    public override void InstallBindings()
    {
        Container.BindInstances (LevelSettings);
        Container.BindInterfacesTo<LevelDrawer3D> ().AsSingle ();
    }
}