using Sokoban.Grid;
using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "GameSettingsInstaller", menuName = "Installers/GameSettingsInstaller")]
public class GameSettingsInstaller : ScriptableObjectInstaller<GameSettingsInstaller> {
    
    public GridManager.Settings GridSettings;

    public override void InstallBindings() {
        Container.BindInstances (GridSettings);
        Container.BindInterfacesTo<GridManager> ().AsSingle ();
    }
}