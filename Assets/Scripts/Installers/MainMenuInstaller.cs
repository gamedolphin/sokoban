using Sokoban.Levels;
using UnityEngine;
using Zenject;

public class MainMenuInstaller : MonoInstaller<MainMenuInstaller>
{
    public override void InstallBindings()
    {
        Container.Bind<LevelParser> ().AsSingle ().NonLazy ();
    }
}