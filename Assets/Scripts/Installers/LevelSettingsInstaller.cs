using Sokoban.Levels;
using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "LevelSettingsInstaller", menuName = "Installers/LevelSettingsInstaller")]
public class LevelSettingsInstaller : ScriptableObjectInstaller<LevelSettingsInstaller>
{
    public LevelDrawer.Settings LevelSettings;

    public override void InstallBindings()
    {
        Container.BindInstances (LevelSettings);
        Container.BindInterfacesTo<LevelDrawer> ().AsSingle ();
    }
}