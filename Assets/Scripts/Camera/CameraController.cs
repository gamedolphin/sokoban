﻿using Sokoban.Grid;
using UnityEngine;
using Zenject;

namespace Sokoban.Cam {
    [RequireComponent(typeof(Camera))]
    public class CameraController : MonoBehaviour {

        [Inject]
        private GridManager gridManager;

        private Camera mainCamera;

        private void Awake () {
            mainCamera = GetComponent<Camera> ();
        }

        public void RecenterCamera (int width, int height) {
            float centerX = ( (float) width - 1 ) / 2;
            float centerY = ( (float) height - 1 ) / 2;
            var centerPosition = gridManager.GetPosition (centerX, centerY);
            transform.position = new Vector3(centerPosition.x, centerPosition.y, transform.position.z);
            mainCamera.orthographicSize = width * gridManager.GridSettings.XWidth;
        }
    }
}
