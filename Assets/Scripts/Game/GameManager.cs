﻿using UnityEngine;
using Sokoban.Levels;
using Zenject;
using Sokoban.Cam;
using Sokoban.Player;
using Sokoban.Hole;
using Sokoban.GameMenu;

namespace Sokoban.Game {
    public class GameManager : MonoBehaviour {

        [Inject]
        private LevelManager levelManager;
        [Inject]
        private LevelDrawer levelDrawer;
        [Inject]
        private LevelInfo levelInfo;

        public CameraController cameraController;
        public GameMenuManager menuManager;

        private void Start () {
            menuManager.Initialize (this);
            StartLevel ();
        }

        public void StartLevel () {
            var level = levelManager.GetLevel (levelInfo);
            levelDrawer.DrawLevel (this, level);
            cameraController.RecenterCamera (level.LevelWidth, level.LevelHeight);
        }
    }
}
