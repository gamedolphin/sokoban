﻿using Sokoban.Cam;
using Sokoban.GameMenu;
using Sokoban.Levels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Sokoban.Game {
    public class GameManager3D : MonoBehaviour {

        [Inject]
        private LevelManager levelManager;
        [Inject]
        private LevelDrawer3D levelDrawer;
        [Inject]
        private LevelInfo levelInfo;

        private void Start () {
            StartLevel ();
        }

        public void StartLevel () {
            var level = levelManager.GetLevel (levelInfo);
            levelDrawer.DrawLevel (this, level);
        }
    }
}