﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sokoban.Grid {

    public enum GridUnitType {
        WALL, PLAYER, BOX, HOLE
    }
    
    public struct GridPosition {
        public int X;
        public int Y;

        public GridPosition (int x, int y) {
            X = x;
            Y = y;
        }

        public override bool Equals (object obj) {
            GridPosition pos = (GridPosition) obj;
            return pos.X == X && pos.Y == Y;
        }

        public static GridPosition operator + (GridPosition c1, GridPosition c2) {
            return new GridPosition { 
                X = c1.X + c2.X, 
                Y = c1.Y + c2.Y 
            };
        }

        public static GridPosition operator - (GridPosition c1, GridPosition c2) {
            return new GridPosition {
                X = c1.X - c2.X,
                Y = c1.Y - c2.Y
            };
        }
    }

    public struct GridUnit {
        public GridPosition GridPosition;
        public HashSet<GridUnitType> Units;
    }

    public class GridManager {

        private readonly Settings _settings;
        public Settings GridSettings {
            get {
                return _settings;
            }
        }

        private List<List<GridUnit>> grid = new List<List<GridUnit>> ();
        private int maxWidth = 0;
        private int maxHeight = 0;

        public GridManager (Settings _set) {
            _settings = _set;
        }

        public void Initialize (int sizeX, int sizeY) {

            grid.Clear ();
            maxWidth = sizeX;
            maxHeight = sizeY;

            for (int i = 0; i < sizeX; i++) {
                List<GridUnit> verticalList = new List<GridUnit> ();
                for (int j = 0; j < sizeY; j++) {
                    GridUnit gridUnit = new GridUnit { Units = new HashSet<GridUnitType> (),
                        GridPosition = new GridPosition { 
                            X = i,
                            Y = j
                        }
                    };
                    verticalList.Add (gridUnit);
                }
                grid.Add (verticalList);
            }
        }

        public void AddUnit (GridUnitType type, GridPosition pos) {
            if (pos.X < 0 || pos.Y < 0 || pos.X >= maxWidth || pos.Y >= maxHeight ) {
                Debug.Log ("EXCEEDED GRID BOUNDS " + pos.X + ":" + pos.Y + ":" + maxWidth + ":" + maxHeight);
                return;
            }

            grid[pos.X][pos.Y].Units.Add (type);
        }

        public HashSet<GridUnitType> GetUnit (GridPosition pos) {
            int x = pos.X;
            int y = pos.Y;
            if (x < 0 || y < 0 || x >= maxWidth || y >= maxHeight) {
                Debug.Log ("EXCEEDED GRID BOUNDS");
                return null;
            }

            return grid[x][y].Units;
        }

        public GridUnit MoveUnit (GridUnitType t, GridPosition pos, GridPosition dPos, List<GridUnitType> collisionFilter = null) {
            GridUnit defaultReturn =  new GridUnit {
                GridPosition = pos,
                Units = null
            };

            GridPosition posNew = pos + dPos;
            int x = pos.X;
            int y = pos.Y;
            int posNewX = posNew.X;
            int posNewY = posNew.Y;
            if (x < 0 || y < 0 || x >= maxWidth || y >= maxHeight) {
                Debug.Log ("EXCEEDED GRID BOUNDS");
                return defaultReturn;
            }
            if (posNewX < 0 || posNewY < 0 || posNewX >= maxWidth || posNewY >= maxHeight) {
                Debug.Log ("EXCEEDED GRID BOUNDS");
                return defaultReturn;
            }

            HashSet<GridUnitType> units = GetUnit (pos);
            if(!units.Contains(t)) {
                Debug.Log ("BOX DOES NOT CONTAIN THIS UNIT");
                return defaultReturn;
            }

            HashSet<GridUnitType> unitsInNewPos = GetUnit (posNew);
            if (collisionFilter != null) {
                HashSet<GridUnitType> collidedWith = new HashSet<GridUnitType> ();
                collisionFilter.ForEach (filter => {
                    if (unitsInNewPos.Contains (filter)) {
                        collidedWith.Add (filter);
                    }
                });
                if (collidedWith.Count > 0) {
                    defaultReturn.Units = collidedWith;
                    return defaultReturn;
                }
            }

            if(unitsInNewPos.Contains(t)) {
                Debug.Log ("NEW POS ALREADY HAS THIS UNIT");
                return defaultReturn;
            }


            grid[x][y].Units.Remove (t);
            grid[posNewX][posNewY].Units.Add (t);
            defaultReturn.GridPosition = posNew;
            return defaultReturn;
        }

        public Vector2 GetPosition (GridPosition pos) {
            return new Vector2 (pos.X * _settings.XWidth, pos.Y * _settings.YWidth);
        }

        public Vector2 GetPosition (float x, float y) {
            return new Vector2 (x * _settings.XWidth, y * _settings.YWidth);
        }

        public Vector3 GetPosition3D (float x, float z) {
            return new Vector3 (x * _settings.XWidth, 0, z * _settings.YWidth);
        }

        [System.Serializable]
        public class Settings {
            public float XWidth;
            public float YWidth;
            public float movementTime;
        }
    }
}
