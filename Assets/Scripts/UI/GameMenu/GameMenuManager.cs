﻿using Sokoban.Game;
using Sokoban.Hole;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Sokoban.GameMenu {
    public class GameMenuManager : MonoBehaviour {

        [Inject]
        private HoleManager holeManager;

        [SerializeField]
        private Button restartButton;

        [SerializeField]
        private Text scoreText;

        public void Initialize (GameManager manager) {
            restartButton.onClick.AddListener (manager.StartLevel);
            holeManager.FilledHoleCount.Subscribe (UpdateScore);
            holeManager.TotalHoleCount.Subscribe (UpdateScore);
        }

        private void UpdateScore (int n) {
            scoreText.text = "FILLED HOLES: " + holeManager.FilledHoleCount.Value+"/"+holeManager.TotalHoleCount.Value;
        }
    }
}
