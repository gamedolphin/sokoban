﻿using Sokoban.Levels;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using UnityEngine.SceneManagement;
using Zenject;

namespace Sokoban.MainMenu {

    public class LevelCollectionSelectManager : MonoBehaviour {

        [Inject]
        readonly ZenjectSceneLoader _sceneLoader;

        [Inject]
        private DiContainer container;

        [Inject]
        private LevelParser levelParser;

        [SerializeField]
        private HorizontalScrollSnap pageView;
        [SerializeField]
        private int noOfButtonsPerPage = 6;

        [SerializeField]
        private RectTransform levelCollectionPrefab;
        [SerializeField]
        private LevelCollectionButton levelSelectionButton;

        private List<LevelInfo> levelData;
        private const string LEVELSFOLDER = "LevelData";

        private void Awake () {
            levelData = ReadAllFiles ();
        }

        private void Start () {
            CreateList ();
        }

        private void CreateList () {
            int noOfPages = levelData.Count / noOfButtonsPerPage;
            if (levelData.Count % noOfButtonsPerPage != 0) {
                noOfPages += 1;
            }
            int count = 0;
            for (int i = 0; i < noOfPages; i++) {
                var page = Instantiate (levelCollectionPrefab);
                pageView.AddChild (page.gameObject, false);
                for (int j = 0; j < noOfButtonsPerPage; j++) {
                    CreateLevelButton (page.transform, count);
                    count += 1;
                    if (count >= levelData.Count) {
                        break;
                    }
                }
            }
        }

        private void CreateLevelButton (Transform parent, int count) {
            var button = Instantiate (levelSelectionButton);
            button.transform.SetParent (parent);
            button.UIText.text = ( count + 1 ).ToString ();
            button.Initialize (() => {
                OnButtonClick (count);
            });
        }

        private void OnButtonClick (int n) {
            LevelInfo level = levelData[n];
            SwitchScene (level);
        }

        private List<LevelInfo> ReadAllFiles () {
            TextAsset[] levelList = Resources.LoadAll<TextAsset> (LEVELSFOLDER);
            List<LevelInfo> levelInfo = new List<LevelInfo> ();
            foreach (var levelText in levelList) {
                SokobanLevels levelData = levelParser.LoadLevelData (levelText);
                levelData.Levels.ForEach (level => {
                    levelInfo.Add (new LevelInfo {
                        fileName = levelText.name,
                        levelID = level.Id
                    });
                });
            }
            return levelInfo;
        }

        private void SwitchScene(LevelInfo level) {
            _sceneLoader.LoadScene (k.Scenes.GAME_SCENE3D, LoadSceneMode.Single,  (container) => {
                container.BindInstance (level).WhenInjectedInto<GameInstaller> ();
            });
        }
    }
}
