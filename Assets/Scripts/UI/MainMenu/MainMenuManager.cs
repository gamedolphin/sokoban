﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using DG.Tweening;

namespace Sokoban.MainMenu {
    public class MainMenuManager : MonoBehaviour {

        [SerializeField]
        private HorizontalScrollSnap mainScrollControl;
        [SerializeField]
        private Button playButton;
        [SerializeField]
        private Button backButtonFromLevelSelect;

        private void Awake () {
            playButton.onClick.AddListener (() => {
                OnButtonClick (1);
            });
            backButtonFromLevelSelect.onClick.AddListener (() => {
                OnButtonClick (0);
            });
        }

        private void OnButtonClick (int n) {
            mainScrollControl.GoToScreen (n);
        }
    }
}
