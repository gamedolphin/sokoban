﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Sokoban.MainMenu {
    [RequireComponent(typeof(Button))]
    public class LevelCollectionButton : MonoBehaviour {
        
        public Text UIText;

        private Button Button;
        
        public void Initialize (UnityAction callback) {
            if(Button == null) {
                Button = GetComponent<Button> ();
                Button.onClick.AddListener (callback);
            }
        }
    }
}
