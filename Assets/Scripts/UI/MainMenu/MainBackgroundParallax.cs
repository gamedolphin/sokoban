﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.Extensions;

namespace Sokoban.MainMenu {
    [RequireComponent(typeof(RectTransform))]
    public class MainBackgroundParallax : MonoBehaviour {

        [SerializeField]
        private HorizontalScrollSnap mainScrollControl;

        private RectTransform mainBackground;

        Tween backgroundTween;

        private void Start () {
            mainBackground = GetComponent<RectTransform> ();
            mainScrollControl.OnSelectionChangeStartEvent.AddListener (OnChangeMainPage);
        }
        
        private void OnChangeMainPage () {
            if(backgroundTween != null) {
                backgroundTween.Kill ();
            }
            int currentPage = mainScrollControl.CurrentPage; // this is before the page has changed
            if (currentPage == 0) {
                // we're switching to the other page
                backgroundTween = mainBackground.DOPivotX (0.5f, 0.5f).SetEase (Ease.OutQuad);
            }
            else {
                backgroundTween = mainBackground.DOPivotX (0, 0.5f).SetEase (Ease.OutQuad);
            }
        }

    }
}
