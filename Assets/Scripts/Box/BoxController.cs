﻿using Sokoban.Grid;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using DG.Tweening;
using Sokoban.Hole;

namespace Sokoban.Box {
    public class BoxController : MonoBehaviour {

        [Inject]
        private HoleManager holeManager;

        [Inject]
        private BoxManager boxManager;

        [Inject]
        private GridManager gridManager;

        public GridPosition Position;

        private List<GridUnitType> collisionFilters = new List<GridUnitType>();

        private void Awake () {
            collisionFilters.Add (GridUnitType.WALL);
            collisionFilters.Add (GridUnitType.BOX);
        }

        public void Initialize (int x, int y) {
            Position = new GridPosition(x, y);
            boxManager.AddBox (this);
        }

        public GridUnit Move(GridPosition dPos) {
            GridUnit collisionResponse = gridManager.MoveUnit (GridUnitType.BOX, Position, dPos, collisionFilters);
            if(collisionResponse.Units == null) {
                MoveBox (collisionResponse.GridPosition);
            }
            return collisionResponse;
        }

        private void MoveBox(GridPosition posNew) {
            if(posNew.Equals(Position)) {
                return;
            }
            holeManager.OnBoxMove (Position, posNew);
            Position = posNew;
        }

        private void OnDestroy () {
            boxManager.RemoveBox (this);
        }
    }
}
