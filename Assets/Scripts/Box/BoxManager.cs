﻿using Sokoban.Grid;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sokoban.Box {
    public class BoxManager {

        private List<BoxController> boxes = new List<BoxController> ();

        public void AddBox (BoxController box) {
            boxes.Add (box);
        }

        public void RemoveBox(BoxController box) {
            boxes.Remove (box);
        }

        public BoxController FindBox (GridPosition pos) {
            return boxes.Find (box => {
                return box.Position.Equals (pos);
            });
        }
    }
}
